from django.urls import path
from accounts.views import user_login, user_logoff, signup


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logoff, name="logout"),
    path("signup/", signup, name="signup"),
]
